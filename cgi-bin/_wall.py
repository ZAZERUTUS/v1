#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import random
import time


class Wall:
    USERS = 'cgi-bin/user.json'
    WALL = 'cgi-bin/wall.json'
    COOKIES = 'cgi-bin/cookies.json'
    
    def __init__(self):
        '''Create file'''
        try:
            with open (self.USERS, 'r', encoding='utf-8'):
                pass
        except FileNotFoundError:
            with open(self.USERS, 'w', encoding='utf-8') as f:
                json.dump({'admin': 'admin'}, f)
       
        try:
            with open (self.WALL, 'r', encoding='utf-8'):
                pass
        except FileNotFoundError:
            with open(self.WALL, 'w', encoding='utf-8') as f:
                json.dump({'__user__': []}, f)
                
        try:
            with open (self.COOKIES, 'r', encoding='utf-8'):
                pass
        except FileNotFoundError:
            with open(self.COOKIES, 'w', encoding='utf-8') as f:
                json.dump({}, f)
    
    def find(self, user, password=None):
        with open(self.USERS, 'r', encoding='utf-8') as f:
            users = json.load(f)
        if user in users and (password is None or password == users[user]):
            return True
        return False
            
    def open_json(self, file_name, w_r, new_data=None):
        print(f'input data - {file_name}\n{w_r}\n{new_data}')
        with open(file_name, w_r, encoding='utf-8') as f:
            if w_r == 'r':
                return json.load(f)
            elif w_r == 'w':
                return json.dump(new_data, f)
            
    def register(self, user, password):
        if self.find(user):
            return False
        users = self.open_json(self.USERS, 'r')
        users[user] = password
        self.open_json(self.USERS, 'w', users)
        return True
        
    def set_cookie(self, user):
        cookies = self.open_json(self.COOKIES, 'r')
        cookie = str(time.time()) + str(random.randrange(10**14))
        cookies[cookie] = user
        self.open_json(self.COOKIES, 'w', cookies)
        return cookie
        
    def find_cookie(self, cookie):
        cookies = self.open_json(self.COOKIES, 'r')
        return cookies.get(cookie)
        
    # def publish(self, user, text):
    #     wall = self.open_json(self.WALL, 'r')
    #     wall['posts'].append({'user': user, 'text': text})
    #     self.open_json(self.WALL, 'w', wall)

    def publish(self, user, text):
        wall = self.open_json(self.WALL, 'r')
        if wall.get(user, None) is None:
            wall.update({user: []})
        wall[user].append(text)
        self.open_json(self.WALL, 'w', wall)
        
    # def html_list(self):
    #     wall = self.open_json(self.WALL, 'r')
    #     posts = []
    #     for post in wall['posts']:
    #         content = post['user'] + ' : ' + post['text']
    #         posts.append(content)
    #     return '<br>'.join(posts)

    def html_list(self, user):
        wall = self.open_json(self.WALL, 'r')
        posts = []
        try:
            if user != 'admin':
                for id_post, post in enumerate(wall[user]):
                    content = f'<p>My post({user}) : {post} <input name="del_post" type="submit" action="delete" value={user}___{id_post}></p>'
                    posts.append(content)
                for pst in wall['admin']:
                    content = f'<p>ADMIN POST - {pst}</p>'
                    posts.append(content)
            else:
                for user, item in wall.items():
                    for i in item:
                        content = f'<p>User - {user} : {i}</p>'
                        posts.append(content)
            return ''.join(posts)
        except KeyError:
            return ''

    def dell_post_admin(self, user_and_id):
        data = user_and_id.split('___')
        wall = self.open_json(self.WALL, 'r')
        wall[data[0]][data[1]] = 'POST DELETED ADMIN'
        self.open_json(self.WALL, 'w', wall)

        
